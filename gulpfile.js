var gulp = require('gulp');
var stylus = require('gulp-stylus');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var rename = require("gulp-rename");
var cssMinify = require("gulp-minify-css");
var browserify = require('gulp-browserify');

gulp.task('default', function() {
	// place code for your default task here

	var watcherSt = gulp.watch('./assets/css/**/*.styl', ['stylus']);
	watcherSt.on('change', function(event) {
		console.log('Stylus: File '+event.path+' was '+event.type+', running tasks...');
	});

	var watcherJs = gulp.watch('./assets/js/**/*.js', ['js']);
	watcherJs.on('change', function(event) {
		console.log('Scripts: File '+event.path+' was '+event.type+', running tasks...');
	});
});

gulp.task('stylus', function(){

	gulp.src('./assets/css/*.styl')
		//.pipe(cssMinify({keepSpecialComments: 0}))
		//.pipe(concat('style.min.css'))
		.pipe(stylus({use: ['nib'], import: ['nib'] set: ['compress']}))
		.pipe(rename(function (path) {
			path.basename += ".min";
		}))
		.pipe(gulp.dest('./css/'));

});

gulp.task('js', function(){

	gulp.src('./assets/js/**/*.js')
		.pipe(browserify({
			insertGlobals : true,
			debug : false,
			transform: ['hbsfy'],
			shim: {	
				'jquery': {
					path: 'assets/bower/jquery/jquery.min.js',
					exports: null
				},
				'jquery-render-tweets': {
					path: './assets/bower/jquery-render-tweets/lib/jquery.render-tweets.js',
					exports: 'jquery-render-tweets',
					exports: null,
					depends: {
						jquery: 'jQuery',
					}
				}
		}
		}))
		.pipe(rename(function (path) {
			//path.dirname += "/js";
			path.basename += ".min";
		}))
		//.pipe(concat('script.min.js')) // Queremos conservar un fichero js para cada kata
		.pipe(uglify()) 
		.pipe(gulp.dest('./js/'));
});