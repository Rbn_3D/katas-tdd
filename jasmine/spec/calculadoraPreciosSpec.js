describe("CalculadoraPrecio: Descuentos", function() {
	var calc;

	beforeEach(function() {
		calc = new CalculadoraPrecio();
	});

	it(" si no tenemos libros diferentes, el descuento debe ser de un 0%", function(){
		expect(calc.getDescuento(1)).toEqual(0);
	})

	it(" si tenemos 2 libros diferentes, el descuento debe ser de un 5%", function(){
		expect(calc.getDescuento(2)).toEqual(0.05);
	})

	it(" si tenemos 3 libros diferentes, el descuento debe ser de un 10%", function(){
		expect(calc.getDescuento(3)).toEqual(0.10);
	})

	it(" si tenemos 4 libros diferentes, el descuento debe ser de un 20%", function(){
		expect(calc.getDescuento(4)).toEqual(0.20);
	})

	it(" si compramos todos los libros (5), el descuento debe ser de un 25%", function(){
		expect(calc.getDescuento(5)).toEqual(0.25);
	})

});

describe("CalculadoraPrecio: Calcular precio", function() {
var calc;

	beforeEach(function() {
		calc = new CalculadoraPrecio();
	});

	it("si tenemos 1 como cantidad de libros y 1 como el nº de libros diferentes el precio de ser de 8€", function(){
		expect(calc.calcularPrecio(1,1)).toEqual(8);
	})

	it("si tenemos 3 como cantidad de libros y 2 como el nº de libros diferentes el precio de ser de 23.2€", function(){
		expect(calc.calcularPrecio(3,2)).toBeCloseTo(23.2);
	})		
});

describe("CalculadoraPrecio: Calculo final", function() {
var calc;

	beforeEach(function() {
		calc = new CalculadoraPrecio();
	});

	it("Si compro el libro 2, el precio debe ser de 8€", function(){
		expect(calc.getPrecioTotal([2])).toBeCloseTo(8);
	})

	it("si compramos 2 ejemplares del libro 3 el precio de ser de 16€", function(){
		expect(calc.getPrecioTotal([3,3])).toBeCloseTo(16);
	})		

	it("si compramos los libro 2, 4, 5 el precio de ser de 21.6€", function(){
		expect(calc.getPrecioTotal([2,4,5])).toBeCloseTo(21.6);
	})	

	it("si compramos los libro 5, 2, 4 el precio de ser de 21.6€", function(){
		expect(calc.getPrecioTotal([5,2,4])).toBeCloseTo(21.6);
	})

	it("si compramos toda la colección (5), el precio debe ser de 30€", function(){
		expect(calc.getPrecioTotal([1,2,3,4,5])).toBeCloseTo(30);
	})	

	it("si compramos 1,2,3,2,1, el precio debe ser de 36.8€", function() {
		expect(calc.getPrecioTotal([1,2,3,2,1])).toBeCloseTo( ((8*3)*(1-.10)) + ((8*2)*(1-.05)) );
	})	

	it("si compramos 2 copias del 1er libro, 2 del 2º, 2 del 3º, 1 del 4º y 1 del 5º, el precio debe ser de 51.20€", function(){
		expect(calc.getPrecioTotal([1,1,2,2,3,3,4,5])).toBeCloseTo(51.2);
	})

	it(" Si compramos [1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4, 4, 5, 5, 5, 5], el precio debe ser de 141,2", function(){
		expect(calc.getPrecioTotal([1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4, 4, 5, 5, 5, 5])).toBeCloseTo(141.2);
	})	

});