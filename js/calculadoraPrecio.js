
function Utils()
{
}

Utils.arrayContains = function (arr, obj)
{
	return arr.indexOf(obj) != -1;
}

Utils.arrayRevome = function (arr, i)
{
	arr.splice(i, 1);
}

Utils.arrayRevomeByVal = function (arr, obj)
{
	arr.splice(arr.indexOf(obj), 1);
}

Utils.tieneRepetidos = function(arr) {
    var i = arr.length, j, val;

    while (i--) {
    	val = arr[i];
    	j = i;
    	while (j--) {
    		if (arr[j] === val) {
    			return true;
    		}
    	}
    }
    return false;
}

Utils.arrayUnique = function(array) {
    var arr = [];
    for(var i = 0; i < array.length; i++) {
        if(!Utils.arrayContains(arr, array[i])) {
            arr.push(array[i]);
        }
    }
    return arr; 
}

Utils.arrayDiff = function (a1, a2) {
  var a=[], diff=[];
  for(var i=0;i<a1.length;i++)
    a[a1[i]]=true;
  for(var i=0;i<a2.length;i++)
    if(a[a2[i]]) delete a[a2[i]];
    else a[a2[i]]=true;
  for(var k in a)
    diff.push(parseInt(k));
  return diff;
}



var CalculadoraPrecio = function()
{
	this.precioUnitario = 8;
	this.libros = new Array();

	this.minimoDescuento = 2;
	this.maximoDescuento = 5;

	this.descuentos = [];

	this.descuentos.push({cantidad: 2, descuento: .05});
	this.descuentos.push({cantidad: 3, descuento: .10});
	this.descuentos.push({cantidad: 4, descuento: .20});
	this.descuentos.push({cantidad: 5, descuento: .25});
}

CalculadoraPrecio.prototype.ordenarLibros = function()
{
	this.libros.sort(function (a,b) { return a-b; });
}

CalculadoraPrecio.prototype.getDescuento = function(nDiferentes) {
	var descuento = 0;

	for (var i = 0; i < this.descuentos.length; i++) {
		if(this.descuentos[i].cantidad == nDiferentes)
			return this.descuentos[i].descuento;     
	}

	return descuento;
}

CalculadoraPrecio.prototype.getEfectividad = function(nDiferentes) {
	var efectividad = 0;

	for (var i = 0; i < this.descuentos.length; i++) {
		if(this.descuentos[i].cantidad == nDiferentes)
			return this.descuentos[i].efectividad;
	}

	return efectividad;
}

CalculadoraPrecio.prototype.calcularPrecio = function(totalLibros, numeroDiferentes) {
	var precioFijos = 0;
	var precioRebajados = 0;

	var nFijos = totalLibros - (numeroDiferentes==1?0:numeroDiferentes);

	for(var i = 0; i < nFijos; i++)
		precioFijos+=this.precioUnitario;

	if(numeroDiferentes > 1)
		precioRebajados = (this.precioUnitario * numeroDiferentes) * (1-this.getDescuento(numeroDiferentes));

	return precioFijos + precioRebajados;
}

CalculadoraPrecio.prototype.getPrecioFase = function(libros)
{
	if(!libros.length)
		return 0;

	this.libros = libros;
	var acumulado = 0;
	this.ordenarLibros();

	var totalLibros = this.libros.length;
	var numeroDiferentes = 1;

	for (var i = 0; i < this.libros.length; i++) {
		if(i > 0 && this.libros[i] != this.libros[i-1])
			numeroDiferentes++;
	};

	return this.calcularPrecio(totalLibros, numeroDiferentes);
}

CalculadoraPrecio.prototype.getPrecioTotal = function(libros)
{
	var l = libros.slice(0);
	var totalAcum = 0;

	var grupos = this.crearGrupos(l);

	console.log(grupos);

	this.calcularEfectividad(this.descuentos);
	var grupos = this.optimizarGrupos(grupos);

	console.log(grupos);

	for (var i = 0; i < grupos.length; i++) {
		totalAcum+= this.getPrecioFase(grupos[i]);
	};

	return totalAcum;
}

CalculadoraPrecio.prototype.optimizarGrupos = function(grupos)
{
	if(grupos.length <= 1)
		return grupos;

	var gruposDesc = this.ordenarGrupos(grupos);
	var gruposAsc = gruposDesc.slice();
	gruposAsc.reverse();

	var grupo1 = { coll: gruposDesc, index: 0 };
	var grupo2 = { coll:  gruposAsc, index: 0 };

	var terminado = false;

	while (!terminado) {

		try{
			var grupoPerd = grupo1;
			var grupoGan = grupo2;

			var efectividadPrevista = this.getEfectividad(grupoPerd.coll[grupoPerd.index].length-1) + this.getEfectividad(grupoGan.coll[grupoGan.index].length+1);
			var efectividadPrevistaInversa = this.getEfectividad(grupoPerd.coll[grupoPerd.index].length+1) + this.getEfectividad(grupoGan.coll[grupoGan.index].length-1);

			if(efectividadPrevistaInversa < efectividadPrevista) {
				grupoPerd = grupo2;
				grupoGan = grupo1;
			}

			if(grupoGan.coll[grupoGan.index].length >= this.maximoDescuento) {
				grupoGan.index++;
			}

			if(grupoPerd.coll[grupoPerd.index].length <= this.minimoDescuento) {
				grupoPerd.index++;
			}

			if(grupoPerd.coll[grupoPerd.index] == grupoGan.coll[grupoGan.index])
				 terminado = true;

			if(!terminado)
				this.moverLibro(grupoPerd, grupoGan);
		}
		catch(err) {
			console.log(err);
			terminado = true;
		}
	}

	return grupos;
}

/*
    Valores de respuesta:

	 1 Avanzar Grupo grande
	 0 No mover
	-1 Avanzar grupo pequeño 

 */

CalculadoraPrecio.prototype.moverLibro = function (grupoPerd, grupoGan)
{
	var grupoPerdedor = grupoPerd.coll[grupoPerd.index];
	var grupoGanador  = grupoGan.coll[grupoGan.index];

	var candidatos = Utils.arrayDiff(grupoPerdedor, grupoGanador); // Libros del Grupo pequeño que no están en el grande

	if(candidatos.length) {
		grupoGanador.push(candidatos[0]);
		Utils.arrayRevomeByVal(grupoPerdedor, candidatos[0]);
		ret = candidatos[0];

		if(grupoGanador.length >= this.maximoDescuento)
			grupoGan.index++;

		if(grupoPerdedor.length <= this.minimoDescuento || grupoPerdedor.length == grupoGanador.length)
			grupoPerd.index++;
	}
	else
		grupoPerd.index++;
}

CalculadoraPrecio.prototype.calcularEfectividad = function(tablaDesc)
{
	for (var i = 0; i < tablaDesc.length; i++) {
		tablaDesc[i].efectividad = tablaDesc[i].cantidad / tablaDesc[i].descuento;
	};

	tablaDesc.sort(function (a, b) {
		if(a.efectividad == b.efectividad)
			return b.cantidad - a.cantidad;

		return a.efectividad - b.efectividad;
	});
}

CalculadoraPrecio.prototype.ordenarGrupos = function(grupos)
{
	grupos.sort(function(a, b) {
		return b.length - a.length;
	});

	return grupos;
}

CalculadoraPrecio.prototype.crearGrupos = function(arr)
{
	var grupos = [arr];
	var first = arr;

	while (Utils.tieneRepetidos(first)) {
		var grupo = this.crearGrupo(first);
		grupos.push(grupo);
	}

	return grupos;
}

CalculadoraPrecio.prototype.crearGrupo = function(arr)
{
	var nuevoArr = [];

	for (var i = 0; i < arr.length; i++) {
		if(!Utils.arrayContains(nuevoArr, arr[i])) {
			nuevoArr.push(arr[i]);
			Utils.arrayRevome(arr, i);
			i--;
		}
	};

	return nuevoArr;
}